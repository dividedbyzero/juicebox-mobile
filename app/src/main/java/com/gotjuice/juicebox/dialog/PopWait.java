package com.gotjuice.juicebox.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.gotjuice.juicebox.R;
import com.gotjuice.juicebox.task.SendOrderTask;

/**
 * Created by User on 8/9/15.
 */
public class PopWait {

    Context context;
    TextView text;
    Button yes;

    public PopWait(Context context) {
        this.context = context;
    }

    public PopWait Create(String name) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.popwait);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        text = (TextView) dialog.findViewById(R.id.popwait_text);
        yes = (Button) dialog.findViewById(R.id.popwait_yes);

        text.setText("Your order, " + name + ", is now being processed. Please for the notifation.");
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { dialog.dismiss(); }
        });

        dialog.show();

        return this;
    }

}
