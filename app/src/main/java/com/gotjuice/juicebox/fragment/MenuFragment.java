package com.gotjuice.juicebox.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gotjuice.juicebox.R;
import com.gotjuice.juicebox.UserProfile;
import com.gotjuice.juicebox.menu.MenuDrawerAdapter;
import com.gotjuice.juicebox.menu.MenuDrawerModel;
import com.gotjuice.juicebox.menu.RecycleTouchListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 8/8/15.
 */
public class MenuFragment extends Fragment {

    private static String[] titles = new String[]{"Profile", "About", "Logout"};
    private static String open = "open";
    private static String close = "close";

    private RecyclerView recyclerview;
    private ActionBarDrawerToggle toggle;
    private DrawerLayout drawerlayout;
    private MenuDrawerAdapter adapter;
    private View containerview;
    private FragmentDrawerListener drawerlistener;

    public MenuFragment() { }

    public void setDrawerListener(FragmentDrawerListener listener) { this.drawerlistener = listener; }
    public static List<MenuDrawerModel> getData() {
        List<MenuDrawerModel> data = new ArrayList<>();
        for (int i = 0; i < titles.length; i++) {
            MenuDrawerModel navItem = new MenuDrawerModel();
            navItem.setTitle(titles[i]);
            data.add(navItem);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.menufragment, container, false);
        recyclerview = (RecyclerView) layout.findViewById(R.id.drawerList);

        adapter = new MenuDrawerAdapter(getActivity(), getData());
        recyclerview.setAdapter(adapter);
        recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerview.addOnItemTouchListener(new RecycleTouchListener(getActivity(), recyclerview, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                drawerlistener.onDrawerItemSelected(view, position);
                drawerlayout.closeDrawer(containerview);

                switch (position) {
                    case 0:
                        Intent i = new Intent(getActivity(), UserProfile.class);
                        startActivityForResult(i, 0);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return layout;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerview = getActivity().findViewById(fragmentId);
        this.drawerlayout = drawerLayout;
        toggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.open, R.string.close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        this.drawerlayout.setDrawerListener(toggle);
        this.drawerlayout.post(new Runnable() {
            @Override
            public void run() {
                toggle.syncState();
            }
        });

    }

    public static interface ClickListener {
        public void onClick(View view, int position);
        public void onLongClick(View view, int position);
    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }
}
