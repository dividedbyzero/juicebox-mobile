package com.gotjuice.juicebox.menu;

/**
 * Created by User on 8/8/15.
 */
public class MenuDrawerModel {

    private boolean showNotify;
    private String title;

    public MenuDrawerModel() {}
    public MenuDrawerModel(boolean showNotify, String title) {
        this.showNotify = showNotify;
        this.title = title;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
