package com.gotjuice.juicebox.menu;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

import com.gotjuice.juicebox.fragment.MenuFragment;

/**
 * Created by User on 8/8/15.
 */
public class RecycleTouchListener implements RecyclerView.OnItemTouchListener {

    private GestureDetector gesturedetector;
    private MenuFragment.ClickListener clicklistener;

    public RecycleTouchListener(Context context, final RecyclerView recyclerView, final MenuFragment.ClickListener clickListener) {
        this.clicklistener = clickListener;
        gesturedetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                if (child != null && clickListener != null) {
                    clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

        View child = rv.findChildViewUnder(e.getX(), e.getY());
        if (child != null && clicklistener != null && gesturedetector.onTouchEvent(e)) {
            clicklistener.onClick(child, rv.getChildPosition(child));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}
