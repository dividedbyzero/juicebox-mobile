package com.gotjuice.juicebox;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.gotjuice.juicebox.fragment.MenuFragment;
import com.gotjuice.juicebox.tab.SlidingTabLayout;
import com.gotjuice.juicebox.tab.ViewPagerAdapter;
import com.gotjuice.juicebox.task.GetAllRecipeTask;

import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Main extends AppCompatActivity implements MenuFragment.FragmentDrawerListener {

    private Toolbar toolbar;
    private TextView textview;
    private MenuFragment menudrawer;

    private ViewPager pager;
    private ViewPagerAdapter adapter;
    private SlidingTabLayout tabs;
    private CharSequence Titles[] = { "Today", "Recipes" };
    private int tabnumber = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Initialize();
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {

    }

    void Initialize() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        textview = (TextView) findViewById(R.id.title);

        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("");

        menudrawer = (MenuFragment) getSupportFragmentManager().findFragmentById(R.id.drawfragment);
        menudrawer.setUp(R.id.drawfragment, (DrawerLayout) findViewById(R.id.drawer_layout), toolbar);

        textview.setText(getResources().getString(R.string.app_name).toUpperCase());
        Typeface font = Typeface.createFromAsset(getAssets(),  "fonts/Raleway-ExtraBold.ttf");
        textview.setTypeface(font);

        menudrawer.setDrawerListener(this);

        adapter =  new ViewPagerAdapter(getSupportFragmentManager(), Titles, tabnumber, toolbar, Main.this);
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(android.R.color.white);
            }
        });
        tabs.setViewPager(pager);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    void sample() {



    }
}
